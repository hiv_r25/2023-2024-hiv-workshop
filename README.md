# Workshop Parts
- [Data Science Workshops](data_science/data_science_TOC.md)
- Statistics Workshops
- [Assay Workshops](assays/assay_TOC.md)

# Computing Environments
- [DCC OnDemand URL](https://dcc-ondemand-01.oit.duke.edu/)
- [Getting started with DCC OnDemand](misc/ondemand_howto.md)

# Workshop Content
- [Initial download of workshop content](data_science/reproducible/git_cloning.md)
- [Update workshop content](data_science/reproducible/git_pull.md)

**This website can be reached at: https://duke.is/hiv-r25**

![QR Code for this Gitlab Repo](misc/images/qrcode_duke_is_hiv_r25.png){width=30% height=30%}

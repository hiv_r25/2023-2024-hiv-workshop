
# Reproducible Analysis
- [Reproducible Analysis Lecture](reproducible/reproducible_research_lecture.md)
- Git
    - [Git Introduction](reproducible/git_overview.md)
    - [Git Remotes](reproducible/git_remotes.md)
    - [Git Collaboration](reproducible/git_collaboration.md)
    - [Git Conflicts](reproducible/git_conflicts.md)
    - [Git Clone](reproducible/git_cloning.md)
    - [Git Pull](reproducible/git_pull.md)

# Intro to R
- [R demo](introduction/00_R_demo_HIV.Rmd)
- [Basic R](introduction/01_introduction_to_R.Rmd)
    
# Capstone Workshop
- [Working Through Ugly Spreadsheets](capstone/longitudinal_data_cleanup.Rmd)

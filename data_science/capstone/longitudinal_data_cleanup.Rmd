---
title: "Quantitative Methods for HIV Researchers"
subtitle: "Taming Wild Spreadsheets"
author: "Josh Granek (based on Janice M. McCarthy's original)"
date: "`r Sys.Date()`"
output: html_document
---

# Load Libraries
```{r setup}
library(purrr)
library(dplyr)
library(tidyr)
library(ggplot2)
library(stringr)

library(readxl)
library(openxlsx)
```


# Background
This exercise is based on a dataset available from the HIV Sequence Database at Los Alamos National Laboratory. This [longitudinal dataset](https://www.hiv.lanl.gov/content/sequence/HIV/SI_alignments/set10.html) tracks the co-evolution of HIV virus and the host immune response over several years of HIV infection in a single individual. The dataset contains several types of data for each of the timepoints:

1. CD4+ T-cell counts
2. CD8+ T-cell counts
3. Viral load
4. Viral sequences
5. Immune specificity for different viral epitopes, measured by ELISpot of Cytotoxic T-lymphocytes (CTL))

We will be working with several of the Excel spreadsheets in the dataset. The formatting of these spreadsheets is not ideal for data analysis, so most of the work will be in data clean and reorganization. Our goal is to reproduce parts of some of the figures from [this paper](https://pubmed.ncbi.nlm.nih.gov/21283794/), which was published based on this dataset. 
[HIV sequence database]() at 

# Download data
First we will download and unpack the data. We will write code below to download the data to our accounts on DCC.

## Download to Local Computer
You might also want to download the data to your local computer so you can use Excel to view the files we will be using. To download the data, go to the webpage for [Special Interest Data Set 10](https://www.hiv.lanl.gov/content/sequence/HIV/SI_alignments/set10.html), and click on the link for 

[Download data](https://www.hiv.lanl.gov/content/sequence/HIV/SI_alignments/data/set10_PIC1362_Liu.zip). Once the file is downloaded to your local computer, try double click on it to unpack it.

## Download to DCC
### Setup Paths

First let's assign the URL for the data to a variable, and do the same for files and directories.
```{r}

```



### Download and Unzip
Next let's download and unpack the zip file containing the data.
```{r}

```

Let's look at what files are in the dataset
```{r}
```

We are going to looking at the spreadsheets ("xls" and "xlsx") files.


Let's also assign paths of the files we will work with to variables
```{r}

```

# Load Data to Dataframe

## Clinical Data
Let's start with an easy dataset - the CD4, CD8, and viral counts as a function of time.

Load it into a dataframe and then plot the clinical values as a function of "days post first onset of acute symptoms"
```{r}

```

```{r}

```

```{r}

```

Where are all the CD4 values? 

If we look at the data we see that the ranges of CD4/CD8 values are much lower than viral load (they are measuring two different types of things).

There are a few ways to deal with this. A simple one is to facet the variables and allows the facets to use separate y-axis.


```{r}

```

That seems to be an improvement!

# Two Row Header: Strongly Selected Epitopes
Let's take a look at another file: strongly_selected_epitopes.xlsx

This file has the response of cytotoxic T-cell (collected at the different timepoints from the study-subject's blood) to several HIV epitopes.

The formatting is kind of annoying because the header is in two rows.

Let's try loading it to a dataframe and see what we get

```{r}

```

It looks like a mess! The best approach might be to extract the header rows, fix them, then add them to the rest of the dataframe.


## Fixing the header
Getting the header rows is pretty easy.

```{r}

```
Let's also get rid of the first column because we are going to deal with that separately (it doesn't really have a header).

```{r}

```

Ack! Not what we wanted. The ranges option has overridden our 'n_max' option. But we can extract what we want from this - the first two rows contain the column names.


```{r}

```
Now let's work on combining the two rows of each header into one
```{r}

```

## Reading the data
Now we are ready to extract the data into a tibble. We'll just skip the first 3 rows, and give read_excel the header (column names) we just made. We are also adding "DPS" for the first column name (this is indicated below the table)

```{r}

```


Almost there. We notice that the read function decided that DPS is a character variable. We can fix this by telling the read function that we want DPS to be an integer value by setting the `col_types` option.

> Any idea why `read_excel` thinkgs the first column should be type "character"?


```{r}

```

One last thing: We noticed that the last two rows of the .xlsx file are not data. They are one blank row and one row that notes DPS stands for 'days post onset of acute symptoms'. Might as well remove these last two rows:


```{r}

```

## Visualizing the data
Now let's have some fun! Let's plot this data.
```{r}

```
This is pretty hard to interpret, let's see if we can improve it!

```{r fig.height = 6}

```

## Exercise
Find other files in the dataset with this same header structure and try to load them!


# Header with Merged Cells
Now, onto the next file type. Here we have an added layer of complexity: A THREE row header. But it gets even better, because this additional row contains merged cells

## Fixing the header
Let's see what happens when we try to do a simple parsing of the spreadsheet
```{r}

```

Definitely not good, the merged cells are split and the label goes to only the first cell of the formerly merged cells, the rest get `NA`, so we lose the header label. This is going to be very ugly to manage with read_excel. Fortunately we have another option!!

### openxlsx
openxlsx is another package for reading Excelspreadsheets into R, and it provides a mechanism to manage merged cells.

```{r}

```

The merged cells are still split (a dataframe has no mechanism for "merged cells"), but all the cells that were formerly merged inherit the content of the former merged cell.

We still have a lot of work to do, but we have tackled this sort of problem before!
```{r}

```
This is pretty good. We have improved the mess alot!

But, there are still some ugly bits:
  1. DPS_DPS_DPS
  2. Header labels ending in "_NA", and " "
  3. Headers containing " _"
  4. Mix of " " and "_" in headers
  
Let's see if we can clean these up

```{r}

```
Beautiful!

## Loading Data
Now let's load the excel spreadsheet with our new vector as header
```{r}

```

## Visualizing the data
Now let's have a look at the data
```{r}

```

Well, that was disappointing! We can't see anything here because the legend is *HUGE*. Let's try again

```{r}

```

Better, but it is hard to make any sense of the data, let's try faceting, that helped last time

```{r fig.height=20, fig.width=8}

```

Much better! There is still room for improvement, but at least now we can look at the data!

## Exercise
Find other files in the dataset with this same header structure and try to load them!

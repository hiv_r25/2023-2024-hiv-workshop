# Outline of Workshops

## Day 1
   * Reproducibility, Rmarkdown and git
   * Introduction to the dcc, Rstudio and basic R 
   
## Day 2
   * Basic R continued
   * R package ``dplyr`` - manipulating data frames
   
## Day 3
  * ggplot with real and simulated data
  
## Day 4
   * Review using HIV example
   
## Day 5
   * Experimental design, hypothesis testing
   
## Day 6
   * Capstone or Bring your own data
   

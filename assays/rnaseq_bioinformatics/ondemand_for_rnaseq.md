# Starting up RStudio on DCC OnDemand
1. Go to [DCC OnDemand](https://dcc-ondemand-01.oit.duke.edu/)
2. Sign in with your NetID and password
3. Click on **Interactive Apps** at the top and select **CHSI RStudio Workshop** 
    - If you don't see **Interactive Apps** and only see three lines in the top right, click on the three lines, then **Interactive Apps**, then **CHSI RStudio Workshop**
4. A new page should open that says **CHSI RStudio Workshop** at the top. Change the following values:
  - *CPUs:* 16
  - *Memory:* 64
5. Scroll to the bottom and click **Launch**.
5. A new page should open with a box that says **CHSI RStudio Workshop**, you may see the following messages:
    - "Please be patient as your job currently sits in queue. The wait time depends on the number of cores as well as time requested."
    - "Your session is currently starting... Please be patient as this process can take a few minutes."
6. You are waiting for a blue button to appear that says **Connect to RStudio Server**. This could take a minute or so. When it appears, click on it.
7. After a minute or so, RStudio should open in your web browser.

# Shutting Down
RStudio and the container it is running in will continue running if you close your web browser and even if you shut off your local computer. However when the reservation time (Walltime selected when you started the container) has elapsed, RStudio and the container will be shut down without warning. You can see how much time remains for your reservation by clicking on **My Interactive Sessions** on the [DCC OnDemand](https://dcc-ondemand-01.oit.duke.edu/) page.

If you know you are done using RStudio and your container for now, it is a good idea to manual shut them down. It is unlikely that anything terrible will happen if you don't, but unsaved changes will probably be lost, and the next time you open RStudio it will probably complain that it wasn't shut down properly last time.

## Closing RStudio
1. In RStudio click on **File** in the top left, then select **Quit Session**
2. A box should pop up that says **R Session Ended**. At this point you can close the tab that contains the RStudio session.

## Closing Container
1. On the [DCC OnDemand](https://dcc-ondemand-01.oit.duke.edu/) page, click on **My Interactive Sessions**.
2. In the **CHSI RStudio Workshop** box click on the red **Delete** button , then click **Confirm** in the box that pops up.
3. You can now click on the logout button in the top right, which is an arrow pointing to the right.
4. You are done!


**My Interactive Sessions** on the [DCC OnDemand](https://dcc-ondemand-01.oit.duke.edu/) page.

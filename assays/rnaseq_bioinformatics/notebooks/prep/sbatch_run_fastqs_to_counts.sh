#!/usr/bin/env bash

#------------------------------------------------
SCRIPTDIR="$( dirname -- "${BASH_SOURCE[0]}"; )";   # Get the directory name
export SCRIPTDIR="$( realpath -e -- "$SCRIPTDIR"; )";    # Resolve its full path if need be
set -u
#------------------------------------------------


# This script will trim and count all the FASTQs in the data directory
# It uses sbatch array, so it is fast (total run time of about 2 minutes)

# This script depends on the following scripts:
#   - batch_setup.Rmd
#   - batch_subscript.sh
#   - batch_run.Rmd

export SINGULARITY_IMAGE="/opt/apps/community/od_chsi_rstudio/hiv_r25_rstudio.sif"
export BIND_ARGS="--bind /work:/work --bind /hpc/group:/hpc/group"

export BATCH_THREADS=8
export BATCH_TOT_MEM=10G

RAW_FASTQS="/hpc/group/hiv-r25-2023/data/rnaseq/sra_fastqs"

FASTQ_R1_FILES=($(ls -1tr ${RAW_FASTQS}/*.fastq.gz))


srun -A chsi -p chsi singularity exec $BIND_ARGS $SINGULARITY_IMAGE Rscript -e "rmarkdown::render('$SCRIPTDIR/batch_setup.Rmd')"

echo "Total Jobs: ${#FASTQ_R1_FILES[@]}"
sbatch -W --array=1-${#FASTQ_R1_FILES[@]} -A chsi -p chsi --mem=${BATCH_TOT_MEM} -c ${BATCH_THREADS} $SCRIPTDIR/batch_subscript.sh

#!/usr/bin/env bash
set -u

# This is run by sbatch_run_fastqs_to_counts.sh through an sbatch array

singularity exec $BIND_ARGS $SINGULARITY_IMAGE Rscript -e "rmarkdown::render('$SCRIPTDIR/batch_run.Rmd')"

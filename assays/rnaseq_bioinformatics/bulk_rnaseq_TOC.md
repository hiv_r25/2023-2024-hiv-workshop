[Bulk RNA-Seq Lecture](rnaseq_bioinformatics_lecture.pdf)
  
# Bulk RNA-Seq Bioinformatic Steps
  - [Quality Control](notebooks/fastqc.Rmd)
  - [Trimming and Filtering](notebooks/fastq_trimming.Rmd)
  - [Download and Index Genome](notebooks/genome_prep.Rmd)
  - [Mapping Reads to a Reference Genome](notebooks/mapping.Rmd)
    
# Appendix
  - [Quality Score Explanation](notebooks/quality_scores.md) ([Rmd](notebooks/quality_scores.Rmd))
  - [Download SRA Data](notebooks/prep/download_data_sra.Rmd)
  - [Make Adapter File](notebooks/prep/make_adapter_fasta.Rmd)
  - [Run Full Pipeline](notebooks/run_everything.Rmd)
  - Analyze Full Dataset
    - [Rmarkdown Version](notebooks/prep/all_fastqs_to_counts.Rmd)
    - [SLURM (Command-line) Version](notebooks/prep/sbatch_run_fastqs_to_counts.sh)
    
## Computing Environment
  - [Introduction to Open OnDemand](ondemand_for_rnaseq.md)

## Bash Intro
  - [Setup for SWC Unix Lessons](../../misc/unix_shell_setup.Rmd)
  - [Software Carpentry: The Unix Shell](https://swcarpentry.github.io/shell-novice/)



#!/bin/bash
#
#SBATCH -A hiv-r25-2023
#SBATCH -p chsi-high
#SBATCH -c 4
#SBATCH --mem=64G

# Set dir with util scripts -- ASSUMES SPECIFIC DIR STRUCTURE WITHIN HOME DIR!!!
export SCRIPT_DIR="/hpc/home/${USER}/project_repos/hiv_r25/2023-2024-hiv-workshop/assays/sc-rnaseq/util"
echo "Current working path: $SCRIPT_DIR"
export BIND_ARGS="/work:/work,/hpc/group/hiv-r25-2023:/hpc/group/hiv-r25-2023"
export CONTAINER_SRA="/work/ts415/images/test-pull-sra_latest.sif"
export CONTAINER_CELLRANGER="/work/ts415/images/rstudio-singularity-2022-2023-hiv_latest.sif"

CELLRANGER_THREADS=8
CELLRANGER_MEM="96G"

# 1: Get metdata from GEO and SRA
singularity exec --bind ${BIND_ARGS} ${CONTAINER_SRA} Rscript -e "rmarkdown::render('${SCRIPT_DIR}/01-get_metadata.Rmd')"

# 2: Download SRA
singularity exec --bind ${BIND_ARGS} ${CONTAINER_SRA} Rscript -e "rmarkdown::render('${SCRIPT_DIR}/02-download_data_sra.Rmd')"

# 3: Run cellranger count using SLURM array
echo "Running cellranger multi"
export FASTQ_DIR="/hpc/group/hiv-r25-2023/rawdata/PRJNA823380/fastq_dir"
export ARRAY_N=$(ls -l ${FASTQ_DIR}/*R1_001.fastq.gz | wc -l) #Number of unique samples
echo "$ARRAY_N samples will run!"

# echo "Total Jobs: ${ARRAY_N}"
sbatch -W --array=1-${ARRAY_N} -A hiv-r25-2023 -p chsi-high --mem=${CELLRANGER_MEM} -c ${CELLRANGER_THREADS} "${SCRIPT_DIR}/cellranger_count_batch_subscript.sh"

#!/usr/bin/env bash
set -u

singularity exec --bind ${BIND_ARGS} ${CONTAINER_CELLRANGER} Rscript -e "rmarkdown::render('${SCRIPT_DIR}/03-run_cellranger.Rmd')"

---
title: "Download Data"
output:
  html_document:
    toc: false
---

This script can be run on Duke Compute Cluster with the following after cd-ing into the directory that contains this file
`srun \
  -A chsi -p chsi \
  -c10 --mem 100G \
  singularity exec --bind /work oras://gitlab-registry.oit.duke.edu/mic-course/2024-mic-singularity-image:v000 \
  Rscript -e "rmarkdown::render('download_sra_prefetch.Rmd')"`


# Parameters

- GEO: GSE200164
- BioProject: PRJNA823380

# Setup

## Load Libraries
```{r}
library(GEOquery)
library(dplyr)
library(rentrez)
library(readr)
library(stringr)
library(fs)
library(R.utils)
library(purrr)
library(tibble)
library(tools)
library(tidyr)
library(here)
```

```{r}
here::here("assays",
           "sc_rnaseq",
           "config.R"
  ) %>%
  source()

dir.create(prj_dir, showWarnings = FALSE, recursive = TRUE)
```

## SLURM Variables
```{r}
Sys.getenv("SLURM_CPUS_PER_TASK") %>% # use as many cpus as were allocated by SLURM
  as.integer %>%
  suppressWarnings ->
  num_cpus
if (is.na(num_cpus)){num_cpus=1} # use 1 CPU if $SLURM_CPUS_PER_TASK is not defined (or is not an integer)

Sys.getenv("SLURM_MEM_PER_NODE") %>% # use as much memory as allocated by SLURM
  as.integer %>%
  suppressWarnings ->
  ram_mb
if (is.na(ram_mb)){ram_mb=2000} # use 2GB CPU if $SLURM_MEM_PER_NODE is not defined (or is not an integer)

num_cpus
ram_mb
```


## Auto-Calculated Paths
```{r}
# sra_tmp=file.path("/workspace", "sra_tmp"); dir.create(sra_tmp, recursive = TRUE)
# prj_dir=path.expand("~/scratch")
temp_dir=file.path(prj_dir, "temp_dir"); dir.create(temp_dir, recursive = TRUE)

sra_cache=file.path(temp_dir, "sra_cache")
# sra_tmp=file.path(prj_dir, "sra_tmp"); dir.create(sra_tmp, recursive = TRUE)
fqdump_tmp=file.path(temp_dir, "fqdump_tmp"); dir.create(fqdump_tmp, recursive = TRUE)
fastq_dir=file.path(prj_dir, "fastq_dir"); dir.create(fastq_dir, recursive = TRUE)

ncbi_config = path.expand("~/.ncbi/user-settings.mkfg")
sra_metadata_csv = file.path(data_dir,"sra_metadata.csv")

srafastq_md5file = here("info/sra_fastq_md5checksums.txt")
```

# Make Metadata Table with SRA and GEO Accesion Numbers
## Get SRA Metadata
This chunk doesn't need to be run because info is loaded from a file in the repo
```{r}
if (file_exists(sra_metadata_csv)){
  print("Loading metadata from local file")
  sra_metadata_csv %>%
    read_csv ->
    sra_metadata
} else {
  print("Downloading metadata from NCBI")

  entrez_search(db="sra", term=bioproject_accession,retmax=1000) ->
    sra_search
  
  # check that we got everything
  stopifnot(sra_search[["count"]]==length(sra_search[["ids"]]))
  
  entrez_fetch(db="sra", id=sra_search[["ids"]], rettype="runinfo") %>%
    read_csv ->
    sra_metadata
  
  sra_metadata %>%
    write_csv(sra_metadata_csv)
}
```

# Download FASTQs from SRA
## Make NCBI config File

  - https://github.com/ncbi/sra-tools/issues/409#issuecomment-801344783
    - https://github.com/ncbi/sra-tools/blob/e2117adf073f748cc48412c55acdc9bfe679a2d1/build/docker/Dockerfile.delite#L50-L52    
```{r}
if(file.exists(ncbi_config)){
    print(paste("NCBI config exists:", ncbi_config))

    ncbi_config %>%
        read_file %>%
        cat
} else {
    print(paste("Need to make NCBI config:", ncbi_config))
#     mkdir -p $(basename $NCBI_CONFIG)
    ncbi_config %>%
        dirname %>%
        dir.create(recursive = TRUE)

    system2("vdb-config", 
        args = paste0("--set ", '"/LIBS/GUID =', uuid::UUIDgenerate(),'"'),
        stdout = TRUE,stderr = TRUE)
    system2("vdb-config", 
        args = paste0("--set ", '"/repository/user/main/public/root =', sra_cache,'"'),
        stdout = TRUE,stderr = TRUE)

    ncbi_config %>%
        read_file %>%
        cat
}
```

# Using Prefetch
## Function Definitions
### Prefetch
```{bash eval=FALSE, include=FALSE}
prefetch --help
```

prefetch --progress --output-directory $PREFETCH_DIR $PREFETCH_ACC
  -r|--resume <yes|no>             Resume partial downloads - one of: no, yes
                                     [default]

```{r}
sraPrefetch = function(accession,outdir,logdir){

  prefetch_args = c("--output-file", file.path(outdir, paste0(accession,".sra")),
                     accession,
                     "--max-size 100g") # "--output-directory", outdir,
  log_path_base = file.path(logdir, paste(c(accession,"prefetch"), collapse="_"))
  status_log = file.path(logdir, "status_log.csv")
  system2(command="prefetch",
          args=prefetch_args,
          stdout = paste0(log_path_base, "_stdout.log"),
          stderr = paste0(log_path_base, "_stderr.log")
          ) ->
    exit_status
  
  write_csv(tibble(srr_accession = accession,
                   stage="prefetch", 
                   status=exit_status),
              file=status_log,
              append=TRUE)
  
  return(exit_status)
}
```

### fasterq-dump
```{bash eval=FALSE, include=FALSE}
fasterq-dump --help
```

fasterq-dump \
  --outdir $SRA_DIR \
  --temp $SRA_TMP \
  $PREFETCH_DIR/$PREFETCH_ACC


  -b|--bufsize <size>              size of file-buffer (dflt=1MB, takes
                                     number or number and unit where unit is
                                     one of (K|M|G) case-insensitive)
  -c|--curcache <size>             size of cursor-cache (dflt=10MB, takes
                                     number or number and unit where unit is
                                     one of (K|M|G) case-insensitive)
  -m|--mem <size>                  memory limit for sorting (dflt=100MB,
                                     takes number or number and unit where
                                     unit is one of (K|M|G) case-insensitive)
  -e|--threads <count>             how many threads to use (dflt=6)

```{r}
fasterqDump = function(accession,prefetch_dir,outdir,tempdir,logdir,
                       threads=NULL,
                       memory=NULL){
  srafile_path=file.path(prefetch_dir, paste0(accession,".sra"))

  fasterqdump_args =  c("--outdir", outdir,
                        "--temp", tempdir,
                        srafile_path,
                        "--split-files",             #Return R1, R2 and index files
                        "--include-technical"
  )
  
  if(is.integer(threads)){
    
    fasterqdump_args = c(fasterqdump_args,
                         "--threads", threads)
  }
  if(is.integer(memory)){
    
    fasterqdump_args = c(fasterqdump_args,
                         "--mem", paste0(ram_mb,"M")
    )
  }
  
  log_path_base = file.path(logdir, paste0(accession,"_fasterqdump"))
  status_log = file.path(logdir, "status_log.csv")
  print(fasterqdump_args)
  system2(command="fasterq-dump",
          args=fasterqdump_args,
          stdout = paste0(log_path_base, "_stdout.log"),
          stderr = paste0(log_path_base, "_stderr.log")
          ) ->
    exit_status
  
  write_csv(tibble(srr_accession = accession,
                   stage="fasterqdump", 
                   status=exit_status),
              file=status_log,
              append=TRUE)
  
  return(exit_status)
}
```

### Compress
```{bash eval=FALSE, include=FALSE}
pigz --help
```

pigz --processes $NUM_THREADS $SRA_DIR/${PREFETCH_ACC}_[12].fastq
```{r}
fastqCompress = function(accession,fastq_dir,logdir,threads=1){
  fastq_paths = list.files(path=fastq_dir,
                           pattern=paste0(accession,"_[1234].fastq$"),
                           full.names=TRUE)

  pigz_args =  c("--processes", threads,
                 fastq_paths)
  
  log_path_base = file.path(logdir, paste0(accession,"_pigz"))
  status_log = file.path(logdir, "status_log.csv")
  system2(command="pigz",
          args=pigz_args,
          stdout = paste0(log_path_base, "_stdout.log"),
          stderr = paste0(log_path_base, "_stderr.log")
          ) ->
    exit_status
  
  write_csv(tibble(srr_accession = accession,
                   stage="pigz", 
                   status=exit_status),
              file=status_log,
              append=TRUE)
  
  return(exit_status)
}
```

### md5sum
```{r}
fastqMD5 = function(accession,fastq_dir,outfile){
  fastq_paths = list.files(path=fastq_dir,
                           pattern=paste0(accession,"_[12].fastq.gz$"),
                           full.names=TRUE)
  fastq_paths %>%
    md5sum %>%
    enframe(name="fastq_path",
            value="computed_md5") %>%
    mutate(dummy="",
           fastq_base=basename(fastq_path)) %>%
    select(computed_md5,dummy,fastq_base) %>%
    write_delim(file=outfile,
                delim=" ",
                append=TRUE)
}
```

### Combine Steps
```{r}
downloadRun = function(run_accession,prefetch_dir,
                       fastq_dir,
                       fqdump_tmp,
                       num_cpus,
                       ram_mb,
                       md5_path,
                       log_dir){
  sraPrefetch(run_accession,prefetch_dir,log_dir)
  fasterqDump(run_accession,
              prefetch_dir,
              outdir=fastq_dir,
              tempdir=fqdump_tmp,
              logdir=log_dir,
              threads=num_cpus,
              memory=ram_mb)

fastqCompress(run_accession,
              fastq_dir,
              logdir=log_dir,
              threads=num_cpus)

fastqMD5(run_accession,fastq_dir,md5_path)
}
```



## Applying It

```{r}
log_dir=file.path(temp_dir, "log"); dir.create(log_dir, recursive = TRUE)
status_log = file.path(log_dir, "status_log.csv")
prefetch_dir=file.path(temp_dir, "prefetch"); dir.create(prefetch_dir, recursive = TRUE)
md5_path=file.path(fastq_dir,"fastq_md5_checksum.txt")

if(dir_exists(log_dir)){
  print("log_dir already exists")
} else {
  dir_create(log_dir, recurse = TRUE)
}
```

```{r eval=FALSE, include=FALSE}
sraPrefetch(run_accession,prefetch_dir,log_dir)
```

```{r eval=FALSE, include=FALSE}
fasterqDump(run_accession,
            prefetch_dir,
            outdir=fastq_dir,
            tempdir=fqdump_tmp,
            logdir=log_dir,
            threads=num_cpus,
            memory=ram_mb)
```

```{r eval=FALSE, include=FALSE}
fastqCompress(run_accession,
              fastq_dir,
              logdir=log_dir,
              threads=num_cpus)
```

fastqMD5 = function(accession,fastq_dir,outfile)

```{r eval=FALSE, include=FALSE}
fastqMD5(run_accession,fastq_dir,md5_path)
```

```{r}
# If a status_log exists, then this must be a rerun!
# we want to figure out which samples ran successfully (all stages have exit status of zero)
# and skip these

if (file_exists(status_log)) {
  print("Old status log exists")
  status_log %>%
    file_info %>%
    pull(modification_time) %>%
    format(format = "%Y%m%d_%H%M") ->
    mod_string
  
  status_log %>%
    read_csv(col_names = c("run_accession", "stage", "exit_status")) ->
    status_log_df
  
  cur_ext = path_ext(status_log)
  status_log %>%
    path_ext_remove %>%
    paste0("_", mod_string) %>%
    path_ext_set(cur_ext) ->
    status_log_new_path
  
  #status_log_df[6,3] = 7
  
  status_log_df %>%
    filter(exit_status == 0) %>%
    group_by(run_accession) %>%
    summarise(num_stages_ok = n()) %>%
    filter(num_stages_ok == 3) %>%
    pull(run_accession) ->
    completed_runs
  
  file_move(status_log, status_log_new_path)
  
} else{
  completed_runs = c()
}
```

```{r}
sra_metadata %>%
  pull(Run) ->
  all_accessions

setdiff(all_accessions, completed_runs) %>%
  map(function(x) downloadRun(run_accession=x,
                              prefetch_dir=prefetch_dir,
                              fastq_dir=fastq_dir,
                              fqdump_tmp=fqdump_tmp,
                              num_cpus=num_cpus,
                              ram_mb=ram_mb,
                              md5_path=md5_path,
                              log_dir=log_dir))
```



# Rename Files

Rename the output files to correspond to [CellRanger formatting specs](https://www.10xgenomics.com/support/software/cell-ranger/latest/analysis/inputs/cr-specifying-fastqs)

```{r}
#Get list of unique SRRs
srr.list <- 
  data.frame(
    SRR = 
      list.files(fastq_dir) %>%
      stringr::str_extract("SRR[0-9]+") %>%
      unique()
  ) %>%
  
  #Add sample index corresponding to "S[N]" in the output FASTQ files
  mutate(sample.index = 1:n())

for (i in 1:nrow(srr.list)) {
  cur.srr = srr.list$SRR[i]
  cur.sample.index = srr.list$sample.index[i]
  
  print(paste("Renaming files in SRR:", cur.srr))
  
  #Find list of files with current SRR that have NOT yet been renamed (based on the pattern)
  fastq.list <- list.files(fastq_dir, pattern = paste0(cur.srr, "_[1234].fastq.gz"))
  
  #Check if there are any FASTQ files to rename
  if (length(fastq.list) > 0) {
    #Loop over files and rename
    for (file in fastq.list) {
      
      #If the suffix is 1 or 2, then cur.file is an index read (I1/I2)
      if(grepl("\\_[12]\\.fastq", file)) {
      
        #Make the formatting base
        new.name = stringr::str_replace(file, 
                                        "(SRR[0-9]+)\\_([12])(\\.fastq\\.gz)", 
                                        "\\1_S1_I\\2_001\\3"
        )
        
        #Replace S1 with the correct sample index
        new.name = stringr::str_replace(new.name, 
                                        "S1", 
                                        paste0("S", cur.sample.index)
        )
        
      
        print(paste("Renaming file:", file, "to:", new.name))
      
        #Rename the file
        file.rename(from = file.path(fastq_dir, file),
                    to = file.path(fastq_dir, new.name)
        )
      }
      
      #If the suffix is 3 then cur.file is a biological read R1
      if(grepl("\\_[3]\\.fastq", file)) {
      
        #Make the formatting base
        new.name = stringr::str_replace(file, 
                                        "(SRR[0-9]+)\\_([3])(\\.fastq\\.gz)", 
                                        "\\1_S1_R1_001\\3"
        )
        
        #Replace S1 with the correct sample index
        new.name = stringr::str_replace(new.name, 
                                        "S1", 
                                        paste0("S", cur.sample.index)
        )
      
        print(paste("Renaming file:", file, "to:", new.name))
        
        #Rename the file
        file.rename(from = file.path(fastq_dir, file),
                    to = file.path(fastq_dir, new.name)
        )
      }
      
      #If the suffix is 4, then cur.file is a biological read R2
      if(grepl("\\_[4]\\.fastq", file)) {
      
        #Make the formatting base
        new.name = stringr::str_replace(file, 
                                        "(SRR[0-9]+)\\_([4])(\\.fastq\\.gz)", 
                                        "\\1_S1_R2_001\\3"
        )
        
        #Replace S1 with the correct sample index
        new.name = stringr::str_replace(new.name, 
                                        "S1", 
                                        paste0("S", cur.sample.index)
        )
      
        print(paste("Renaming file:", file, "to:", new.name))
        
        #Rename the file
        file.rename(from = file.path(fastq_dir, file),
                    to = file.path(fastq_dir, new.name)
        )
      }
    }
  } else {
    print("No FASTQ files to rename!")
  }
}
```


# SessionInfo
```{r}
sessionInfo()
```

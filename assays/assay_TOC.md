
### Introduction to High-throughput Sequencing (3/4/2024)
  - [High-Throughput Sequencing Lecture](hts_background.pdf)
  
### Bioinformatics for Bulk RNA-seq (3/11/2024)
  - [Bulk RNA-Seq Bioinformatics Table of Contents](rnaseq_bioinformatics/bulk_rnaseq_TOC.md)

### Statistical Analysis for RNA-seq  (3/18/2024)

### Background for scRNA-Seq (3/25/2024)
  1. [scRNA-Seq Background Slides](sc_rnaseq/slides/scrna_bioinformatics.pdf)
  2. [Cell Ranger Demo](sc_rnaseq/notebooks/03_day4_cellranger_demo.Rmd)

### scRNA-Seq: Overview of tools; Using Seurat for QC, Transformations, and Normalization (4/1/2024)

### scRNA-Seq: Dimension reduction, Clustering, Cluster Annotation, and Visualization (4/8/2024)
